VPATH = data templates
fn    = Report

output = $(fn).pdf $(fn).R linMod.csv

all: $(output)

$(fn).pdf: $(fn).tex Rpackages.bib
	latexmk -pdf $(fn)
	latexmk -c   $(fn)

$(fn).tex Rpackages.bib : $(fn).Rnw
	Rscript -e "library(knitr); knit('$(fn).Rnw')"
	sed -i -e "s/ &/ \\\\\&/g" Rpackages.bib

$(fn).R: $(fn).Rnw
	Rscript -e "library(knitr);                       \
	            purl('$(fn).Rnw', output = '$(fn).R')"

.PHONY: all clean cleanAll cleanRGuiFiles

cleanAll : clean
	rm -rf .Rproj.user

clean: cleanRGuiFiles
	latexmk -f -C $(fn)
	rm -f $(fn).fdb_latexmk $(fn).fls $(fn).tex $(output)

cleanRGuiFiles:
	rm -rf .Rhistory $(fn).bbl $(fn).synctex.gz \
	       $(fn)-concordance.tex figure framed.sty Rpackages.bib
