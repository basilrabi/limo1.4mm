<<{{chunkLab}},fig.cap='{{fig.cap}}',echo=FALSE,fig.height={{fig.height}},fig.align='center',fig.width={{fig.width}},fig.pos='H',warning=FALSE,out.width='\\linewidth'>>=
  plot(v.Models[[{{i}}]][[2]][[{{j}}]][[2]],
       v.Models[[{{i}}]][[2]][[{{j}}]][[3]],
       col = "black",pch = 20, cex = 2, ylab = "Semivariance", xlab = "Distance")
@